#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "concurrence.h"

/*************************************************************************************************************************
 * Prompt options - Main thread looped function
 * -----------------------------------------------------------------------------------------------------------------------
 * This function is run in the "main" thread - This prompts the user to change delay or bit rate characteristics of this
 * leaky bucket.
**/

void prompt_options (gvars* glob)
{
    char            option [80];
    int             option_number;
    unsigned int    newarg;

    char*           headermsg;

    asprintf(&headermsg,
            "Connect to this bucket using port %d, and it will forward to host \"%s\" (port %d)\n" \
            "Current settings:\n" \
            "- Packet delay:     %d ms\n" \
            "- Packet byte rate: %d Bps (bytes per second)\n" \
            "If you want to change them, type...\n" \
            "1.- To change the Packet byte rate.\n" \
            "2.- To change the Packet delay.\n" \
            "\"exit\" or Ctrl+C to exit.",
            glob->localport_number, glob->remotehost, glob->remoteport_number, glob->delay_ms, glob->rate_bps);

    // CONCURRENT_PUTS is a thread-safe print to stdout.
    CONCURRENT_PUTS (glob, headermsg);
    free (headermsg);

    // Gets a line with the proper option.
    // We prefer to use fgets() instead of scanf() due stability.
    fgets (option, 79, stdin);
    option_number = strtol (option, NULL, 10);

    if (strcasecmp (option, "exit\n") == 0)
        signal_exit (SIGINT);

    switch (option_number)
    {
        case 1:
            // Selects a new byte rate
            CONCURRENT_PUTS (glob, "Select the new byte rate you want to set (in bytes per second): ");
            fgets (option, 79, stdin);
            glob->rate_bps = strtol (option, NULL, 10);
            CONCURRENT_PUTS (glob, "New byte rate set.");
        break;
        case 2:
            // Selects a new delay
            CONCURRENT_PUTS (glob, "Select the new delay you want to set (in ms): ");
            fgets (option, 79, stdin);
            glob->delay_ms = strtol (option, NULL, 10);
            CONCURRENT_PUTS (glob, "New delay set.");
        break;
        default:
            CONCURRENT_PUTS (glob, "Not a valid option.");
    }

    CONCURRENT_PUTS (glob, "\n----------------------------------------------------------------------------------------------");
}

/*************************************************************************************************************************
 * Connection accept thread main function
 * -----------------------------------------------------------------------------------------------------------------------
 * This thread keeps locked until a connection arrives, then opens some more threads for the leaky bucket.
**/

void* main_connaccept (void* args)
{
    // Casting. It's the first thing we shall do.
    gvars* glob = (gvars*) args;

    // Arguments for our newly created thread functions.
    direction localdirection    = (direction) {&(glob->socket_session), &(glob->socket_remote), &(glob->thread_delaylocal), &(glob->thread_creditlocal), &(glob->fromlocal_buffer), glob};
    direction remotedirection   = (direction) {&(glob->socket_remote), &(glob->socket_session), &(glob->thread_delayremote), &(glob->thread_creditremote), &(glob->toremote_buffer), glob};

    // Waits for an incoming connection.
    glob->socket_session = accept (glob->socket_local, NULL, NULL);
    INFO ("Receiving incoming connection.");

    glob->isconnected = 1;

    // Initializes everything for bucket use.
    glob->toremote_buffer   = CBUFFER_INITIALIZE;
    glob->fromlocal_buffer  = CBUFFER_INITIALIZE;

    glob->mutex_localbuff   = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;
    glob->mutex_remotebuff  = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;

    // Opens leaky-bucket threads
    // Bucket Fill threads
    pthread_create (&(glob->thread_droplocal), NULL, main_bucketfill, (void*) &localdirection);
    pthread_create (&(glob->thread_dropremote), NULL, main_bucketfill, (void*) &remotedirection);

    pause ();
}

/*************************************************************************************************************************
 * Bucket leak function
 * -----------------------------------------------------------------------------------------------------------------------
 * This function manages the receive-send game in the communication, given the credit managed in the bucket fill function.
 * As long as there are not enough bytes to send in a burst, it waits patiently for the bucket to fill.
**/
void* main_bucketfill (void* args)
{
    // The main ones.
    direction*  sockdir = (direction*) args;
    gvars*      glob    = sockdir->globals;
    cbuffer*    buffer  = sockdir->circularbuffer;

    // The mutex is INSIDE the circular buffer structure.
    pthread_t*  bucketfill_thread = sockdir->fillthread;
    pthread_t*  creditgen_thread = sockdir->creditthread;

    bucketqueue *qfirst = buffer->qfirst;
    bucketqueue *qlast  = buffer->qlast;

    int         received_bytes;
    char        temp_buffer [BUCKET_SIZE];    // Maximum size to receive = BUCKET_SIZE (defined in basics.h)

    pthread_create (bucketfill_thread, NULL, main_bucketfill, args);
    pthread_create (creditgen_thread, NULL, main_creditgen, args);

    while(1)
	{
        //receive
        received_bytes = recv (*(sockdir->socketfrom), temp_buffer, BUCKET_SIZE, 0);

        pthread_mutex_lock (&(buffer->mutex_buffer));
        push_recv_data (glob, &qfirst, &qlast, temp_buffer, received_bytes);
        pthread_mutex_unlock (&(buffer->mutex_buffer));

        #if defined DEBUGMODE
        fprintf (stderr, "[DEBUG] Received %d bytes.\n", received_bytes);
        #endif
	}
}

/*************************************************************************************************************************
 * Bucket credit generator
 * -----------------------------------------------------------------------------------------------------------------------
 * This function rises the credit given for a bucket leak (IOW the bucket must leak at a maximum rate, given by this credit)
 * This credit is periodically set, and will set the speed this bucket gets leaked.
**/
void* main_bucketdrop (void* args)
{
    // The main ones.
    direction*  sockdir = (direction*) args;
    gvars*      glob    = sockdir->globals;
    cbuffer*    buffer  = sockdir->circularbuffer;

    bucketqueue *qfirst = buffer->qfirst;
    bucketqueue *qlast  = buffer->qlast;

    int         sent_bytes;
    char        temp_buffer [BUCKET_SIZE];    // Maximum size to receive = BUCKET_SIZE (defined in basics.h)
    int         currentcredit;

    while(1)
    {
        pthread_mutex_lock(&(buffer->mutex_credit));

        // Makes sure that the credit given is enough to send a pulse.
        while (buffer->credit < glob->rate_bps)
            pthread_cond_wait(&(buffer->cond_credit), &(buffer->mutex_credit));

		buffer->credit -= glob->rate_bps;
		currentcredit = buffer->credit;
		#if defined DEBUGMODE
        fprintf (stderr, "[DEBUG] Credit drained. Now there are %d credits.\n", buffer->credit);
        #endif
        pthread_mutex_unlock(&(buffer->mutex_credit));

        pthread_mutex_lock(&(buffer->mutex_buffer));
        sent_bytes = pull_recv_data (glob, &qfirst, &qlast, temp_buffer, currentcredit);
        pthread_mutex_unlock(&(buffer->mutex_buffer));

        send (*(sockdir->socketto), temp_buffer, sent_bytes, 0);
        #if defined DEBUGMODE
        fprintf (stderr, "[DEBUG] Sent %d bytes.\n", sent_bytes);
        #endif
    }
}

/*************************************************************************************************************************
 * Bucket credit generator
 * -----------------------------------------------------------------------------------------------------------------------
 * This function rises the credit given for a bucket leak (IOW the bucket must leak at a maximum rate, given by this credit)
 * This credit is periodically set, and will set the speed this bucket gets leaked.
**/
void* main_creditgen (void* args)
{
    // The main ones.
    direction*  sockdir = (direction*) args;
    gvars*      glob    = sockdir->globals;
    cbuffer*    buffer  = sockdir->circularbuffer;

    // The mutex is INSIDE the circular buffer structure.
    // This loop rises the credit in a certain time interval.
    while (1)
	{
        sleep (1);
        pthread_mutex_lock (&(buffer->mutex_credit));
        buffer->credit = glob->rate_bps;
        #if defined DEBUGMODE
        fprintf (stderr, "[DEBUG] Credit generated. Now there are %d credits.\n", buffer->credit);
        #endif
		pthread_mutex_unlock (&(buffer->mutex_credit));

		pthread_cond_broadcast (&(buffer->cond_credit));

	}
}
