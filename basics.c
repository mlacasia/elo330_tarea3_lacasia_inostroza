#include <stdlib.h>
#include <string.h>

#include "basics.h"

/*************************************************************************************************************************
 * Push received data
 * -----------------------------------------------------------------------------------------------------------------------
 * In order to keep arrival data ordered, and to make sure that data have waited here at least the delay time, it's modeled
 * after a double-linked list (a queue). This pushes the received data with some kind of delay information and a timestamp,
 * using gettimeofday() function. This puts the new element at the end of the list.
**/

void push_recv_data (gvars* globals, bucketqueue** bqfirst, bucketqueue** bqlast, char* tempbuffer, int length)
{
    bucketqueue* newelem;
    struct timeval  currentts;

    // Retrieves time info.
    gettimeofday (&currentts, NULL);

    // Creates a new element.
    newelem =                malloc (sizeof (bucketqueue));
    newelem->data =          malloc (sizeof (length));
    newelem->datacursor =    0;
    newelem->length =        length;
    newelem->arrivalts =     currentts;

    newelem->prev =  *bqlast;
    newelem->next =  NULL;

    // If the list is not empty, modifies the previous element to link here.
    // ...and timestamp gets modified too.
    if ((*bqlast) != NULL)
        (*bqlast)->next = newelem;

    // If the list is empty, that means we're inserting the first element (Thanks Captain Obvious, duh).
    if ((*bqfirst) == NULL)
        (*bqfirst) = newelem;

    // Modifies the tail of this list.
    *bqlast = newelem;
}

/*************************************************************************************************************************
 * Pull received data
 * -----------------------------------------------------------------------------------------------------------------------
 * This function extracts some data from the beginning of the double-linked list. Returns 0 if it's empty, or other number
 * with the amount of read data. Locks the thread if called too early.
 * WARNING: Make sure that externbuffer has enough space!
**/

int pull_recv_data (gvars* globals, bucketqueue** bqfirst, bucketqueue** bqlast, char* externbuffer, int amount)
{
    struct timeval  currentts, firstts;
    long int        timedifference;
    int             bytescopied;
    bucketqueue*    unlinking_elem;

    // If the list is empty, nothing to do.
    if ((*bqfirst) == NULL)
        return 0;

    // Gets current "time of day", and makes a difference
    gettimeofday (&currentts, NULL);
    firstts = (*bqfirst)->arrivalts;

    timedifference = (currentts.tv_sec - firstts.tv_sec) * 1000000 + (currentts.tv_usec - firstts.tv_usec) + (1000 * globals->delay_ms);

    // If timedifference is positive, that data has not passed enough time in the bucket. Let's lock this thing until it's time.
    if (timedifference > 0)
        usleep (timedifference);

    // Not enough to unlink the first element => only datacursor is changed.
    if (amount < ((*bqfirst)->length - (*bqfirst)->datacursor))
    {
        bytescopied = amount;
        memcpy (externbuffer, &((*bqfirst)->data[(*bqfirst)->datacursor]), bytescopied);
        (*bqfirst)->datacursor += amount;
    }
    // Else, we need to unlink the first element.
    else
    {
        bytescopied = ((*bqfirst)->length - (*bqfirst)->datacursor);
        memcpy (externbuffer, &((*bqfirst)->data[(*bqfirst)->datacursor]), bytescopied);

        // Unlink takes place.
        unlinking_elem = *bqfirst;
        *bqfirst = (*bqfirst)->next;

        free (unlinking_elem->data);
        free (unlinking_elem);
    }

    return bytescopied;
}
