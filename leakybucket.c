#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>

#include <netinet/in.h>
#include <arpa/inet.h>

#include "concurrence.h"

/*
    Syntax:
    ert_tcp <local port> <remote host> <remote port>

    Strategy:
    1. We have the main thread, where we init all the port and host variables; and locates the remote host.
    2. Then we init the from-local and to-remote sockets (socket(), bind(), listen() and connect() functions).
    3. We open a thread for prompting new parameters for the leaky bucket (1) and other thread to listen TCP connections (2)

    Thread (1) - MAIN
    1. Loops the prompt_options() function, defined in concurrence.h

    Thread (2) - SERVER CONNECTION THREAD
    1. Runs the accept() function. This blocks the thread while no foreign connections are made.
    2. Opens a thread receiving a receive function for each direction (3), (4), and a byte drop thread for each direction (5), (6)
*/

#define ARG_AMOUNT          4

// Prototypes
void syntax_message (char* prog_name);
void prompt_options ();

void printhostdetails (struct addrinfo* hostinfo, int limit);

// Events
void signal_exit (int signo);

// Global variables.
gvars   globals;

/*************************************************************************************************************************
 * Main function
 * -----------------------------------------------------------------------------------------------------------------------
 * Initializes everything. Then, this thread serves as the console prompt, changing the options et al.
**/

int main (int argc, char** argv)
{
    int                 errorcode;
    struct sockaddr_in  serverbinder;

    // Inits all global variables in 0.
    memset (&globals, 0, sizeof(globals));
    globals.delay_ms = 1000;
    globals.rate_bps = 10;

    // ------------------------------------------
    // MUTEXES INITIALIZATIONS
    // ------------------------------------------
    globals.stdout_mutex = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;

    // ------------------------------------------
    // PARAMETER HANDLING
    // ------------------------------------------
    // If not enough / too many arguments, displays a message and exits.
    if (argc != ARG_AMOUNT)
    {
        ERROR ("Not enough parameters supplied.");
        syntax_message (argv[0]);
        return 1;
    }

    // Else, process the arguments.
    globals.remotehost = strdup (argv[2]);
    globals.localport = strdup (argv[1]);
    globals.remoteport = strdup (argv[3]);
    globals.localport_number = strtol (argv[1], NULL, 10);
    globals.remoteport_number = strtol (argv[3], NULL, 10);

    srand(time(0));

    // If one of them detected to be 0...
    if (globals.localport_number == 0)
    {
        INFO ("Local port set to 0.");
    }
    if (globals.remoteport_number == 0)
    {
        INFO ("Remote port set to 0.");
    }

    // ------------------------------------------
    // NETWORK SETUP
    // ------------------------------------------
    // If getting the server info returns different from zero, there was an error.
    errorcode = getaddrinfo (globals.remotehost, globals.remoteport, NULL, &(globals.remoteservinfo));
    if (errorcode != 0)
    {
        ERROR ("Could not get the remote server info, given that remote hostname and port.");
        return 2;
    }

    // Retrieves some IP and host info of the remote server.
    INFO ("Getting some useless remote host information. Please wait.");
    printhostdetails (globals.remoteservinfo, 1);

    // ------------------------------------------
    // SOCKETS INITIALIZATION
    // ------------------------------------------
    // Server
    globals.socket_local    = socket (AF_INET, SOCK_STREAM, 0);

    serverbinder = (struct sockaddr_in) {
        .sin_family         = AF_INET,
        .sin_port           = htons (globals.localport_number),
        .sin_addr.s_addr    = htonl (INADDR_ANY)
    };

    if (bind (globals.socket_local, (struct sockaddr*) &serverbinder, sizeof (serverbinder)) == -1)
    {
        ERROR ("Could not bind the server socket.");
        exit (3);
    }
    if (listen (globals.socket_local, MAX_BACKLOG) == -1)
    {
        ERROR ("Could not set the server socket for incoming connections.");
        exit (3);
    }

    // Client
    globals.socket_remote   = socket (AF_INET, SOCK_STREAM, 0);
    if (connect (globals.socket_remote, globals.remoteservinfo->ai_addr, globals.remoteservinfo->ai_addrlen) == -1)
    {
        ERROR ("Could not connect to the remote server.");
        exit (4);
    }

    // ------------------------------------------
    // THREADS INITIALIZATION
    // ------------------------------------------
    // Starts threads for communication to and from the remote server.
    pthread_create (&globals.thread_connaccept, NULL, main_connaccept, (void*) &globals);   // Thread (2), as said in Strategy.

    // Signal_exit responds to a Ctrl+C event.
    signal (SIGINT, signal_exit);

    // ------------------------------------------
    // PROMPT LOOP
    // ------------------------------------------
    // The main thread allows to modify the transmission rate and delay.
    while (1)
        prompt_options(&globals);

    return 0;
}

/*************************************************************************************************************************
 * Syntax message
 * -----------------------------------------------------------------------------------------------------------------------
 * It is printed when not enough arguments are supplied.
**/

void syntax_message (char* prog_name)
{
    printf (
    "%s: Emulates delay and lower TCP bitrate. \n" \
    "Syntax:\t%s <local port> <remote host> <remote port> \n\n" \
    "This program awaits for a TCP connection from a local port, and \n" \
    "slows down and delays the transmission to and from a remote server.\n\n",
    prog_name, prog_name);
}

/*************************************************************************************************************************
 * Print host details
 * -----------------------------------------------------------------------------------------------------------------------
 * This function serves as a verbose function, that prints the IPs of all the hosts found.
**/
void printhostdetails (struct addrinfo* hostinfo, int limit)
{
    // struct addrinfo has the fields ai_addrlen and ai_addr, which we are going to use to fetch the IP directions.
    char hostip     [25];
    char portnum    [10];

    if (limit > 0 && hostinfo != NULL)
    {
        getnameinfo (hostinfo->ai_addr, hostinfo->ai_addrlen, hostip, 24, portnum, 9, NI_NUMERICHOST | NI_NUMERICSERV);

        printf ("Host found in %s, port %s, type %s.\n", hostip, portnum, (hostinfo->ai_socktype == SOCK_DGRAM) ? "UDP" : "TCP");

        // This function is recursive, in order to save us from dealing with nasty iterations.
        printhostdetails (hostinfo->ai_next, --limit);
    }
}

/*************************************************************************************************************************
 * Signal exit
 * -----------------------------------------------------------------------------------------------------------------------
 * This function is called when a SIGINT (Ctrl+C) is detected. It cleans the workspace and exits the program.
**/

void signal_exit (int signo)
{
    // For some reason, if we don't kill that thread, the port would remain busy after this program is closed.
    pthread_kill (globals.thread_connaccept, SIGINT);
    pthread_kill (globals.thread_droplocal, SIGINT);
    pthread_kill (globals.thread_dropremote, SIGINT);
    pthread_kill (globals.thread_delaylocal, SIGINT);
    pthread_kill (globals.thread_delayremote, SIGINT);
    pthread_kill (globals.thread_creditlocal, SIGINT);
    pthread_kill (globals.thread_creditremote, SIGINT);

    free (globals.remotehost);
    free (globals.localport);
    free (globals.remoteport);

    shutdown (globals.socket_local, SHUT_RDWR);
    shutdown (globals.socket_remote, SHUT_RDWR);
    shutdown (globals.socket_session, SHUT_RDWR);

    freeaddrinfo (globals.remoteservinfo);

    INFO ("Exiting...");
    exit (0);
}
