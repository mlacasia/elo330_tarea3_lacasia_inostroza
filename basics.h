#include <pthread.h>
#include <stdint.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netdb.h>

#define     DEBUGMODE

// Some helper macros.
#define     WARNING(s)          fprintf (stderr, "[WARNING] " s "\n");
#define     ERROR(s)            fprintf (stderr, "[ERROR] " s "\n");
#define     INFO(s)             fprintf (stderr, "[INFO] " s "\n");

#define CONCURRENT_PUTS(g,s)    pthread_mutex_lock (&(g->stdout_mutex)); \
                                puts (s); \
                                pthread_mutex_unlock (&(g->stdout_mutex));

#define BUCKET_SIZE             20 * 1024
#define PULSE_SIZE              10

#define MAX_BACKLOG             1

#define PULSE_PERIOD            1

#define CBUFFER_INITIALIZE      (cbuffer) {NULL, NULL, BUCKET_SIZE, 0, 0, (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER, \
                                (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER, (pthread_cond_t) PTHREAD_COND_INITIALIZER};

// It's some kind of priority queue holding all the incoming data with arrival time information.
typedef struct _bucketqueue
{
    unsigned char*          data;
    unsigned int            datacursor;
    unsigned int            length;
    struct timeval          arrivalts;

    struct _bucketqueue*    prev;
    struct _bucketqueue*    next;
} bucketqueue;

// Model for bucket
typedef struct _cbuffer
{
    // Buffer. To be initialized with malloc.
    bucketqueue*    qfirst;
    bucketqueue*    qlast;

    // Cursors.
    unsigned int    length;
    unsigned int    cursor;
    unsigned int    credit;

    // Mutex for concurrent read/write.
    pthread_mutex_t     mutex_buffer;
    pthread_mutex_t     mutex_credit;
    pthread_cond_t      cond_credit;
} cbuffer;

// Model for global variables (made for concurrency).
typedef struct _globals
{
    // Threads
    pthread_t           thread_connaccept;
    pthread_t           thread_droplocal, thread_dropremote;
    pthread_t           thread_delaylocal, thread_delayremote;
    pthread_t           thread_creditlocal, thread_creditremote;

    unsigned char       isconnected;

    // Mutexes
    pthread_mutex_t     stdout_mutex;
    pthread_mutex_t     mutex_localbuff, mutex_remotebuff;

    // Socket IDs
    int             socket_local;
    int             socket_remote;
    int             socket_session;

    // Host connectivity variables
    char*           remotehost;
    char*           localport;
    int             localport_number;
    char*           remoteport;
    int             remoteport_number;

    // Incoming connection details
    struct sockaddr_in  localdetails;
    socklen_t           localdetails_len;

    // Remote server address info
    struct addrinfo*    remoteservinfo;

    // Time properties.
    unsigned int    delay_ms;
    unsigned int    rate_bps;

    // Bucket array.
    cbuffer         toremote_buffer;
    cbuffer         fromlocal_buffer;

    // Bucke
} gvars;

// Direction argument
typedef struct _direction
{
    int*                socketfrom;
    int*                socketto;

    pthread_t*          fillthread;
    pthread_t*          creditthread;
    cbuffer*            circularbuffer;

    gvars*              globals;
} direction;

// These are defined in basics.c
void push_recv_data (gvars* globals, bucketqueue** bqfirst, bucketqueue** bqlast, char* tempbuffer, int length);
int pull_recv_data (gvars* globals, bucketqueue** bqfirst, bucketqueue** bqlast, char* externbuffer, int amount);
