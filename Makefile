OUTPUT = ert_tcp
SOURCES = leakybucket.c concurrence.c basics.c
HEADERS = basics.h concurrence.h
OBJECTS = leakybucket.o concurrence.o basics.o
CFLAGS = -g
LFLAGS = -pthread
CC = gcc

$(OUTPUT):	$(OBJECTS)
		$(CC) $(OBJECTS) -o $(OUTPUT) $(LFLAGS)

leakybucket.o:	leakybucket.c $(HEADERS)
		$(CC) -c leakybucket.c $(CFLAGS)

concurrence.o: 	concurrence.c $(HEADERS)
		$(CC) -c concurrence.c $(CFLAGS)

basics.o:		basics.c $(HEADERS)
		$(CC) -c basics.c $(CFLAGS)

clean:
		rm -f $(OUTPUT) $(OBJECTS)
